import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyBDKC_8poDYNk_OfJy09CwRjTRPzTawsZI",
  authDomain: "imessage-clone-517f5.firebaseapp.com",
  projectId: "imessage-clone-517f5",
  storageBucket: "imessage-clone-517f5.appspot.com",
  messagingSenderId: "830246427190",
  appId: "1:830246427190:web:5436dfd3a5e6fdfc505697",
  measurementId: "G-LMJ1YQ6NKP"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider(); //for auth signup

export { auth, provider };
export default db;
